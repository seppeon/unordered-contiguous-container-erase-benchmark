# Unordered Contiguous Container Erase Benchmark
Key:
\<TestName\>_\<ContainerLength\>

Note: this is with integer elements. As they are trivial this likely doesn't apply to non-trivial types.

Using libstdc++ static vector (length and array), clang 12 on windows:
---
| Benchmark                               | Time        | CPU         | Iterations |
|-----------------------------------------|-------------|-------------|------------|
| MoveAndPop_16                           | 3476 ns     | 3449 ns     | 194783     |
| MoveAndPop_512                          | 100384 ns   | 100098 ns   | 6400       |
| MoveAndPop_65536                        | 12779241 ns | 12834821 ns | 56         |
| MoveAndPopBatch_16                      | 3287 ns     | 3223 ns     | 213333     |
| MoveAndPopBatch_512                     | 100553 ns   | 100442 ns   | 7467       |
| MoveAndPopBatch_65536                   | 12599566 ns | 12500000 ns | 50         |
| MoveAndPopBatchErase_16                 | 3270 ns     | 3223 ns     | 213333     |
| MoveAndPopBatchErase_512                | 100271 ns   | 100442 ns   | 7467       |
| MoveAndPopBatchErase_65536              | 12901456 ns | 12812500 ns | 50         |
| MoveIteratorThenBatchErase_16           | 3261 ns     | 3223 ns     | 213333     |
| MoveIteratorThenBatchErase_512          | 99575 ns    | 100442 ns   | 7467       |
| MoveIteratorThenBatchErase_65536        | 12804900 ns | 12834821 ns | 56         |
| MoveIteratorThenBatchEraseReverse_16    | 3278 ns     | 3296 ns     | 213333     |
| MoveIteratorThenBatchEraseReverse_512   | 99456 ns    | 97656 ns    | 6400       |
| MoveIteratorThenBatchEraseReverse_65536 | 13409183 ns | 13671875 ns | 64         |

![](static_vector_time.png)

Using libstdc++ std::vector, clang 12 on windows:
---
| Benchmark                               | Time        | CPU         | Iterations |
|-----------------------------------------|-------------|-------------|------------|
| MoveAndPop_16                           | 3613 ns     | 3599 ns     | 186667     |
| MoveAndPop_512                          | 104206 ns   | 104980 ns   | 6400       |
| MoveAndPop_65536                        | 12747729 ns | 12834821 ns | 56         |
| MoveAndPopBatch_16                      | 3411 ns     | 3453 ns     | 203636     |
| MoveAndPopBatch_512                     | 99623 ns    | 97656 ns    | 6400       |
| MoveAndPopBatch_65536                   | 12224525 ns | 12451172 ns | 64         |
| MoveAndPopBatchErase_16                 | 3342 ns     | 3369 ns     | 213333     |
| MoveAndPopBatchErase_512                | 99332 ns    | 98349 ns    | 7467       |
| MoveAndPopBatchErase_65536              | 12443648 ns | 12451172 ns | 64         |
| MoveIteratorThenBatchErase_16           | 3472 ns     | 3442 ns     | 213333     |
| MoveIteratorThenBatchErase_512          | 95815 ns    | 95215 ns    | 6400       |
| MoveIteratorThenBatchErase_65536        | 12442758 ns | 12187500 ns | 50         |
| MoveIteratorThenBatchEraseReverse_16    | 3457 ns     | 3453 ns     | 203636     |
| MoveIteratorThenBatchEraseReverse_512   | 93789 ns    | 92773 ns    | 6400       |
| MoveIteratorThenBatchEraseReverse_65536 | 12428556 ns | 12695312 ns | 64         |

![](vector_time.png)